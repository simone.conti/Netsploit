# kills older instances of msfrcpd
kill `pidof msfrpcd` 2> /dev/null

# Start database service for metasploit and initialize it
service postgresql start
msfdb init

# Start rpc servers for exploiting and backdooring
msfrpcd -p 1234 -P password & pid_c=$!
msfrpcd -p 1235 -P password & pid_bc=$!
